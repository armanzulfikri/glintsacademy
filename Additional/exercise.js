// # Latihan Exercise Practice

// ## 1. Deep Sum

// ### Find the value from this multidimensional Array

// ```javascript
    const multidimensional = [
        [
            [15,7,8], [1,5]
        ],
        [
            [23,17,21,50,12],[7,8,1],[100]
        ],
        [
            [105]
        ],
        [
            [112],[100]
        ]
    ]

    const deepSum = (multidimensional) => {
        let array = multidimensional.map( item => item.map(a => a.reduce( (memo, value)=> memo+= value, 0 )))
        .map(b => b.reduce((acc, cur) => acc+= cur, 0))
        .reduce((acc, cur) => acc+= cur, 0); 
      
      return array;
      }

    //Test Case
    console.log(deepSum(multidimensional));

    // Output : 380
// ```

// ## 2. Here's your exchange

// ### Give the exchange

// javascript
    const retjehan = [100000,50000,20000,5000,1000,500,100]

    const exchange = (doit) => {
         retjehan.forEach(el => {
        console.log(doit)
        if (Math.floor(doit / el) > 0) {
            console.log(`${el}: ${Math.floor(doit / el)}`);
        }
        doit %= el
    })

    }

//     //Test Case
    exchange(185500);
//     // Output
//     /*
//         {
//             100000 : 1,
//             50000 : 1,
//             20000 : 1,
//             5000 : 2,
//             500 : 1
//         }
//     */
//    exchange(377000);
//    // Output
//    /*
//         {
//             100000 : 3,
//             50000 : 1,
//             20000: 1,
//             5000 : 1,
//             1000 : 2
//         }
//    */

// ```

// ## 3. Roman Numeral

// ### Output the Romanian Number up to 5000

// ```javascript
    const converter = (decimal) => {
//         //code here -> NO RECURSIVE!!
const romawi = { M: 1000, CM: 900, D: 500, CD: 400, C: 100, XC: 90, L: 50, XL: 40, X: 10, IX: 9, V: 5, IV: 4, I: 1 };
    let roman = '',
        i;
    for (i in romawi) {
        while (decimal >= romawi[i]) {
            roman += i;
            decimal -= romawi[i];
        }
    }
    console.log(roman);
}


//     //Test Case
    converter(2019)
//     //Output : MMXIX
    converter(1999)
//     //Output : MCMXCXIX
    converter(1777)
//     //Output : MDCCLXXVII

// ```

// ## 4. Find the Modus of the array given

// ### Find the Modus

// ```javascript
    const real = [3,7,1,1,5,2,1,8,9,10,9]
    const composites = [8,8,14,18,28,36,28,15,8,28]

    const modus = (numbers) => {
//         //code here
  var frequency = {}; // array of frequency.
  var maxFreq = 0; // holds the max frequency.
  var modes = [];
        for (i in numbers){
            frequency [numbers[i]] = (frequency[numbers[i]]||0)+1

            

    if (frequency[numbers[i]] > maxFreq) { // is this frequency > max so far ?
        maxFreq = frequency[numbers[i]]; // update max.
      }
    }
  
    for (var k in frequency) {
      if (frequency[k] == maxFreq) {
        modes.push(k);
      }
    }
  console.log(modes);
    return modes;
  }
        
    

//     //Test Case
    modus(real)
//     //Output : 1
    modus(composites)
//     //Output : 8
// ```

// ## 5. Count the Fibonacci Numbers

// ### Print the N first fibonacci and then count and then find the average of them

// // ```javascript
//     const fibonacci = (n) => {
//         // code here
//             let fib = [0, 1];
        
//             for (let i = 2; i <= n; i++) {
//                 fib.push(fib[i - 2] + fib[i - 1]);
//             }
//             let total = fib.reduce((a, b) => { return a + b }, 0)
//             fib.shift()
//             console.log(`Fibbonaci = ${fib}\nTotal = ${total}\nAverage = ${total/n} `);
        
//     }
        

//     //Test Case
    // fibonacci(5)
//     //Output :
//     /*
//         Fibonacci = 1 1 2 3 5
//         Total = 12
//         Average = 2.4
//     */
//    fibonacci(10)
//    //Output :
//     /*
//         Fibonacci = 1 1 2 3 5 8 13 21 34 55
//         Total = 143
//         Averag = 14.3
//     */

//    //OUTPUT MUST BE THE SAME!!
// ```

// ## 6. Encrypt the word

// ### Solve the given code to be a nice word

// ```javascript
    const code = ["alpha","bravo","charlie","delta","echo","foxtrot","golf","hotel","india","juliet","kilo","lima","mike","nano"] //Lanjutkan sendiri

    const solvedWord = (sentence) => {
        //code here
        let words = sentence.split(" ");
        let output = "";
        words.forEach(word => {
            code.forEach(code => {
                if (word == code) {
                    output += word[0].toUpperCase()
                }
            })
        })
        if (!output) {
            console.log(`Code can't be solved`);
        } else {
            console.log(output);
        }
    }
     
        
        
    
    
//     //Test Case
    solvedWord("alpha nano juliet india nano golf")
//     //Output : ANJING (upper case)
    solvedWord("bacot bet dah")
//     //Output : Code can't be solved
// ```

// ## 7. Fruit box

// ### Let's buy some fruits with the given doit

// ```javascript
const fruits = [
    {
        name : "Apple",
        stock : 3,
        price : 15000
    },
    {
        name : "Orange",
        stock : 2,
        price : 10000
    },
    {
        name : "Gomu gomu",
        stock : 10,
        price : 50000
    }
]

const cart = (cashOnHands) => {
    //code here
    let Fruits = {
        Apple: 0,
        Orange: 0,
        "Gomu gomu": 0
    };
    let Changes = cashOnHands;
    while(Changes >= fruits[1].price){
        for(let i = 0; i<fruits.length; i++){
            if(Changes >= fruits[i].price){
                Changes -= fruits[i].price
                Fruits[fruits[i].name] += 1;                    
            }
        }
    }
    console.log(Changes,Fruits)
}

//Test case
cart(100000)
//Output
/*
    {
        Changes : 5000,
        Fruits : {
            Apple : 2,
            Orange : 2
        }
    }
*/
cart(75000)
//Output
/*
{
    Changes : 0,
    Fruits : {
        Apple : 1,
        Orange : 1,
        "Gomu Gomu" : 1
    }
}
*/


// ## 8. Cinema Seats

// ### Book your seat

// | A1  A2  A3 |

// | B1  B2  B3 |

// | C1  C2  C3 |

// ```javascript
//     const seats = [
//         ['X','',''],
//         ['','','X'],
//         ['','x','']
//     ]

//     const showSeats = () => {
//         //code here
//     }
//     const book = (seat) => {
//         //code here
//     }

//     //Test case
//     book('A3')
//     //Output : Thanks! Book A3
//     book('C2')
//     //Output : C2 has already been booked!
//     showSeats()
//     //Output :
//     /*
//     [
//         ['X','','X'],
//         ['','','X'],
//         ['','x','']
//     ]
//     */
// ```

// ## 9. Angkot

// ### How many fare we must pay

// ```javascript
    const angkotRoute = ["Kemanggisan","Kemang","Sleepy","Palmerah","Kuningan"]

    const totalFare = (name, from, to) => {
        const ongkos = 3000
        
    }

//     //Test Case
    totalFare("Fanny", "Kemanggisan", "Sleepy")
//     //Output
//     /*
//         Fanny goes from Kemanggisan to Sleepy, pays Rp. 6000
//     */
   totalFare("Siskae", "Palmerah", "Kemang")
//    //OUtput
//    /*
//     Siskae goes from Palmerah to Kemang, pays RP. 9000
//    */
// ```

// ## 10. Livestocks

// ### Farming time

// ```javascript
    const livestocks = {
        Cow : [
            {
                id : 1,
                type : "Belgian Blue"
            }
        ],
        Chicken : [
            {
                id :1,
                type : "Leghorn"
            }
        ]
    }
    const list = () => {
        //code here
        
    }
    const add = (hewan,tipe) => {
        //code here
        
    }

//     //Test Case
    add("Cow","Angus");
//     //Output : Cow : Angus has been added
    add("Sheep","White Tail");
//     //Output : New! Sheep : White Tail has been added
    add("Cow","Angus");
//     //Output : Cow : Angus already exist
    add("cHiCkEn","Cross Breed")
//     //Output : Chicken : Cross Breed has been added
    list();
//     //Output :
//     /*
//         {
//             Cow : ["Belgian Blue","Angus"],
//             Chicken : ["Leghorn","Cross Breed"],
//             Sheep : ["White Tail"]
//         }
//     */
// ```
