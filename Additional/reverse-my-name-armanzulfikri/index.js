function reverseMyName(str) {
    // put your code here
    
    // for (var i = str.length - 1; i >= 0; i--){
    // str += str[i];
  
    // }
    var splitString = str.split('')
    var reverseArray = splitString.reverse();
    var joinArray = reverseArray.join('')

    return joinArray
}

const Test = (fun, result) => console.log(reverseMyName(fun) === result)

Test("A", "A")
Test("Michael Jackson","noskcaJ leahciM")
Test("Alvian Zachry Faturrahman", "namharrutaF yrhcaZ naivlA")
Test("", "")