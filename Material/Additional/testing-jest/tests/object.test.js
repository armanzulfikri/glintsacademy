const { returnObject } = require('../objects/object')

const quote = {
    id : 1,
    savage : true,
    text : "BARU BANGUN PAGI SEKALI AJA UDA BACIT",
    author : "Fanny",
    deliverTo : [
        "werren",
        "arman",
        "abdul"
    ]
}

describe('Object function', () => {
    test("Quote should return all attr", () =>{
        const tempCode = returnObject(quote);

        expect(tempCode).toHaveProperty("id")
        expect(tempCode).toHaveProperty("savage")
        expect(tempCode).toHaveProperty("text")
        expect(tempCode).toHaveProperty("author")
        expect(tempCode).toHaveProperty("deliverTo")
                
    })
    test("Quote should return undefined", ()=>{
        const tempQuote = undefined;
        expect(tempQuote).toBe(undefined)
    })
    test("Quote should return empty string", ()=>{
        const tempQuote = "";
        expect(tempQuote).toBe("")
    })
    test("Quote => savage should return true", () => {
        const tempQuote = returnObject(quote);

        expect(tempQuote).toHaveProperty("savage",true)
    })
    test("Quote => deliverTo should return array", () => {
        const tempQuote = returnObject(quote);
        
        expect(tempQuote.deliverTo).toEqual(["werren","arman","abdul"])
    })
})

const oddArray = [1, 3, 5, 7, 9, 11, 13];
test('should start correctly', () => {
  expect(oddArray).toEqual(expect.arrayContaining([1, 3, 5, 7, 9]));
});
