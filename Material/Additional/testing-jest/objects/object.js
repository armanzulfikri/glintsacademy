
function returnObject(code){
    return code
}


function generateUsername(){
    const sifat = ["Extrovert","Introvert","Judging","Perceiving","Intuition","Sensing","Logic","Feeling"]
    const hewan = ["Sapi","Ayam","Kambing","Semut","Domba","Kerbau","Bebek","Ikan"]

    let sifatNumber = Math.floor(Math.random() * sifat.length)
    let hewanNumber = Math.floor(Math.random() * hewan.length)
    
    return `${hewan[hewanNumber]} ${sifat[sifatNumber]}`
}

module.exports = {
    returnObject, generateUsername
};