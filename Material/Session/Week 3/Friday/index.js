// Process Argv
// const data = process.argv;
// const command = process.argv[2];
// const params = process.argv.slice(3);
// console.log(data);
// console.log(command);
// console.log(params);

// File System
const fs = require('fs');
const jsonTemp = fs.readFileSync('./data.json', 'utf8');
const parseData = JSON.parse(jsonTemp);

// console.log(parseData);
// command
class FruitTree {
    constructor(id, type, age, fruits, totalFruits, matureAge, stopProducing) {
        this.id = id,
            this.type = type;
        this.age = age;
        this.fruits = fruits || 0;
        this.totalFruits = totalFruits || 0;
        this.matureAge = matureAge;
        this.stopProducing = stopProducing;
    }
}

class AppleTree extends FruitTree {
    constructor(id, type, age, fruits, totalFruits, matureAge, stopProducing) {
        super(id, type, age, fruits, totalFruits, matureAge, stopProducing);
    }
}
class PearTree extends FruitTree {
    constructor(id, type, age, fruits, totalFruits, matureAge, stopProducing) {
        super(id, type, age, fruits, totalFruits, matureAge, stopProducing);
    }
}
class OtherTree extends FruitTree {
    constructor(id, type, age, fruits, totalFruits, matureAge, stopProducing) {
        super(id, type, age, fruits, totalFruits, matureAge, stopProducing);
    }
}

// Static Factory Method
class Tree {
    static listTrees() {
        let listTrees = parseData;
        let tempTrees = [];
        listTrees.forEach(data => {
            // Destructuring Object
            const { id, type, age, fruits, totalFruits, matureAge, stopProducing } = data;
            switch (type) {
                case "Apple":
                    tempTrees.push(new AppleTree(id, type, age, fruits, totalFruits, matureAge, stopProducing))
                    break;
                case "Pear":
                    tempTrees.push(new PearTree(id, type, age, fruits, totalFruits, matureAge, stopProducing))
                    break;
                default:
                    tempTrees.push(new OtherTree(id, type, age, fruits, totalFruits, matureAge, stopProducing))
                    break;
            }
        })
        return tempTrees;
    }
    static addTree(tree) {
        let listTrees = this.listTrees();
        const { id, type, age, fruits, totalFruits, matureAge, stopProducing } = tree;
        switch (type) {
            case "Apple":
                listTrees.push(new AppleTree(id, type, age, fruits, totalFruits, matureAge, stopProducing))
                break;
            case "Pear":
                listTrees.push(new PearTree(id, type, age, fruits, totalFruits, matureAge, stopProducing))
                break;
            default:
                listTrees.push(new OtherTree(id, type, age, fruits, totalFruits, matureAge, stopProducing))
                break;
        }

        this.save(listTrees);
    }
    static save(data){
        fs.writeFileSync('./data.json', JSON.stringify(data,null,2));
        console.log("File has saved.")
    }
}

// console.log(Tree.listTrees());
const create =
{
    id: 3,
    type: "Watermelon",
    age: 1,
    fruits: 0,
    totalFruits: 0,
    matureAge: 5,
    stopProducing: 10,
}

// const createA = {
//     id: 4,
//     type: "Banana",
//     age: 1,
//     fruits: 0,
//     totalFruits: 0,
//     matureAge: 5,
//     stopProducing: 10,
// }
// const createB = {
//     id: 5,
//     type: "Pomegranate",
//     age: 1,
//     fruits: 0,
//     totalFruits: 0,
//     matureAge: 5,
//     stopProducing: 10,
// }
Tree.addTree(create);
// Tree.addTree(createA);
// Tree.addTree(createB);

// const meats = {
//     id : 1,
//     name : "Beef",
//     isCooked : true
// }
// console.log(meats.id)
// console.log(meats.name)
// console.log(meats.isCooked)

// // Destructuring Object
// let { id,name,isCooked } = meats;
// console.log(id);
// console.log(name);
// console.log(isCooked);
