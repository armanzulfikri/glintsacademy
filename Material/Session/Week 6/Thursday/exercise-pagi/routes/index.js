const { Router } = require('express');
const router = Router();
const TeacherRoutes = require('./teacher')
const StudentRoutes = require('./student')
const FeedbackRoutes = require('./feedback')
const ProfileRoutes = require('./profile')
const EmployerRoutes = require('./employer')

router.get('/', (req,res)=>{
    res.status(200).json({
        message : "This is home page thanks."
    })
});

//Routes
router.use('/teachers', TeacherRoutes)
router.use('/students', StudentRoutes)
router.use('/feedbacks', FeedbackRoutes)
router.use('/profiles', ProfileRoutes)
router.use('/employers', EmployerRoutes)

module.exports = router;