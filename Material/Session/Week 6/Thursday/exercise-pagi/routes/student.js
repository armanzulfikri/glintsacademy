const { Router } = require('express');
const router = Router();
const StudentController = require('../controllers/student')

router.get('/', StudentController.list)
router.post('/login', StudentController.login)
router.post('/register', StudentController.register)

module.exports = router;