'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Profile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Profile.belongsTo(models.Student);
    }
  };
  Profile.init({
    image: { 
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Image must be filled thanks."
        }
      }
    },
    gender: { 
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Gender must be filled thanks."
        }
      }
    },
    address: { 
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Address must be filled thanks."
        }
      }
    },
    StudentId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Profile',
  });
  return Profile;
};