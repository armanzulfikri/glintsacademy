'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Teacher extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Teacher.belongsTo(models.Employer);
      Teacher.belongsToMany(models.Student, {through: 'models.Feedback'});
    }
  };
  Teacher.init({
    name: { 
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Name must be filled thanks."
        }
      }
    },
    score: { 
      type : DataTypes.INTEGER,
      validate : {
        notEmpty : {
          msg : "Score must be filled thanks."
        },
        isNumeric : {
          msg : "Score must be a number."
        }
      }
    },
    subject: { 
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Subject must be filled thanks."
        }
      }
    },
    image: { 
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Image must be filled thanks."
        }
      }
    },
    EmployerId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Teacher',
  });
  return Teacher;
};