const { Teacher, Employer } = require('../models')

class TeacherController {
    static async getTeacher(req, res, next) {
        try {
            const result = await Teacher.findAll({
                order: [
                    ['id', 'ASC']
                ],
                include : [
                    Employer
                ]
            })
            res.status(200).json(result);
        }
        catch (err) {
            next(err)
        }
    }

    static async addTeacher(req, res, next) {
        const { name, score, subject, image, EmployerId } = req.body;
        //Menerima dari Middlewares authentication
        // const UserId = req.userData.id
        try {
            const teacher = await Teacher.create({
                name, score, subject, image, EmployerId 
            })

            res.status(201).json(teacher)
        } catch (err) {
            next(err);
        }
    }

    static async deleteTeacher(req, res, next) {
        const id = req.params.id;

        try {
            const result = Teacher.destroy({
                where: {
                    id
                }
            })
            res.status(200).json({
                result,
                msg: "Teacher deleted"
            })
        } catch (err) {
            next(err)
        }
    }
}

module.exports = TeacherController;