const { Profile, Student } = require('../models')

class ProfileController {
    static async getProfile(req, res, next) {
        try {
            const result = await Profile.findAll({
                order: [
                    ['id', 'ASC']
                ],
                include : [
                    Student
                ]
            })
            res.status(200).json(result);
        }
        catch (err) {
            next(err)
        }
    }

    static async addProfile(req, res, next) {
        const { image, gender, address, StudentId } = req.body;
        try {
            const profile = await Profile.create({
                image, gender, address, StudentId 
            })

            res.status(201).json(profile)
        } catch (err) {
            next(err);
        }
    }

    static async deleteProfile(req, res, next) {
        const id = req.params.id;

        try {
            const result = Profile.destroy({
                where: {
                    id
                }
            })
            res.status(200).json({
                result,
                msg: "Profile deleted"
            })
        } catch (err) {
            next(err)
        }
    }
}

module.exports = ProfileController;