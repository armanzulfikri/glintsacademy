const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true
    },
    info : {
        type : String,
        required : true
    },
    UserId : {
        type : Number,
        required : true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('Products', ProductSchema, 'Products')