const { Student } = require('../models') 

class StudentController {
    static list(req, res){
        Student.findAll()
        .then( data => {
            res.render('Students', {Students:data})
        })
        .catch( err => {
            res.render('error', err)
        })
    }

    // static getById(req, res){
    //     Student.findOne({
    //         where: {
    //             id: Number(req.params.id)
    //         }
    //     })
    //     .then( data => {
    //         res.render('Students', {teachers : [data]})
    //     })
    //     .catch( err => {
    //         res.render('error', err)
    //     })
    // }
    
}

module.exports = StudentController