const { Student } = require('../models')

class StudentController {
    static getStudent(req, res) {
        Student.findAll()
            .then(result => {
                // console.log(result);
                // res.send(result)
                res.render('student.ejs', { students: result })
            })
            .catch(err => {
                console.log(err);
            })
    }
    static addFormStudent(req, res) {
        res.render('addStudent.ejs');
    }
    static addStudent(req, res) {
        const { name, major, score } = req.body;
        Student.create({
            name,
            major,
            score
        })
            .then(result => {
                // res.send(result)
                res.redirect('/students')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static findById(req, res) {
        const id = req.params.id;
        Student.findOne({
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }

    static deleteStudent(req, res) {
        const id = req.params.id;
        Student.destroy({
            where: { id }
        })
            .then(() => {
                // res.send("Deleted")
                res.redirect('/students')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static updateStudent(req, res) {
        const id = req.params.id;
        const { name, major, score } = req.body;
        Student.update({
            name,
            major,
            score
        }, {
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }
}

module.exports = StudentController; 