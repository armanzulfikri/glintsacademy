const { Router } = require('express');
const router = Router();

const ProductRoutes = require('./product')

router.get('/', (req,res) => {
    res.render('index.ejs');
})

router.use('/product', ProductRoutes);

module.exports = router;