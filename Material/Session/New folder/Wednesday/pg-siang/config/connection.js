const { Pool } = require('pg');
const pool = new Pool({
    database: 'pg-basic',
    host: 'localhost',
    user: 'postgres',
    password: 'root',
    port: 5432
});

module.exports = pool; 