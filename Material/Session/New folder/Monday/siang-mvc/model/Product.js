const fs = require('fs');

class Product {
    constructor(id, name, category, status, createdAt) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.status = status;
        this.createdAt = createdAt;
    }

    static list() {
        const data = fs.readFileSync('./data.json', 'utf8');
        const parseData = JSON.parse(data);

        let tempData = [];
        parseData.forEach(element => {
            const { id, name, category, status, createdAt } = element;
            tempData.push( new Product(id, name, category, status, new Date(createdAt)));
        });
        return tempData;
    }
    static add(params){
        const products = this.list();
        // const name = params[0];
        // const category = params[1];
        // const status = params[2];

        //Destructuring Array
        const [ name,category,status ] = params;
        let nextId = products[products.length-1].id + 1;
        
        const tempObject = {
            id : nextId,
            name : name,
            category : category,
            status : (status === "true"),
            createdAt : new Date()
        };
        products.push(tempObject);
        this.save(products);
        return `Product ${name} has been added.`
    }
    static delete(params){
        const products = this.list();
        const id = Number(params[0]);

        const tempData = products.filter(product => product.id !== id);
        
        let check = false;
        if(tempData.length === products.length){
            check = true;
        }

        if(check){
            return `Id ${id} not found`;
        }else{
            this.save(tempData);
            return `Id ${id} has been deleted.`;
        }
    }
    static update(params){
        const products = this.list();
        const id = Number(params[0]);
        const name = params[1];

        products.forEach(product => {
            if(product.id === id){
                product.name = name;
            }
        })
        this.save(products);
        return `Id ${id} has been updated.`
    }
    static save(data){
        fs.writeFileSync('./data.json', JSON.stringify(data, null,2));        
    }
}

module.exports = Product;