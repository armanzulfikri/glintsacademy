const fs = require('fs');

class Product {
    constructor(id,name,category,status,createdAt){
        this.id = id;
        this.name = name;
        this.category = category;
        this.status = status;
        this.createdAt = createdAt;
    }

    static list(cb){
        // const data = fs.readFileSync('./data.json', 'utf8');
        fs.readFile('./data.json', 'utf8', (err, data) => {
            if(err){
                cb(err,null);
            }
            else {
                const parseData = JSON.parse(data);

                let tempData = [];
                parseData.forEach(data => {
                    const { id, name, category, status, createdAt } = data;
                    tempData.push(new Product(id, name, category, status, new Date(createdAt)));
                });
                cb(null, tempData);
            }
        })
       
        // return tempData;
    }
    static add(params, cb){
        // const products = this.list();
        this.list((err,data) => {
            if(err){
                cb(err,null)
            }
            else {
                const products = data;
                //Destructuring Array
                const [ name,category,status ] = params;
        
                const nextId = products[products.length-1].id + 1;
                const tempObject = {
                    id : nextId,
                    name : name,
                    category : category,
                    status : (status === 'true'),
                    createdAt : new Date()
                }
                products.push(tempObject);
                this.save(products);
                
                cb(null,`Product ${name} has been added.`)
            }
        })

        // this.save(products);
        // return `Product ${name} has been added.`
    }
    static delete(params){
        // console.log("List");
        const products = this.list();
        const id = Number(params[0]);

        const tempData = products.filter((product) => product.id !== id);

        this.save(tempData);
        return `Id ${id} has been deleted`;
    }
    static update(params){
        // console.log("List");
        const products = this.list();
        const id = Number(params[0]);
        const name = params[1];

        products.forEach(product => {
            if(product.id === id){
                product.name = name;
            }
        });
        this.save(products);
        return `Id ${id} has been updated`
    }
    static save(data){
        fs.writeFileSync('./data.json', JSON.stringify(data, null,2));
    }
}

module.exports = Product;