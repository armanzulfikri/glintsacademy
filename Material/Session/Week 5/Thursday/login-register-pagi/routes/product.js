const { Router } = require('express');
const router = Router();
const ProductController = require('../controllers/Product')

const { authentication } = require('../middlewares/auth')

router.get('/', authentication, ProductController.getProduct)
router.post('/', authentication, ProductController.addProduct)

module.exports = router;