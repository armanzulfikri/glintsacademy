const { Router } = require('express');
const router = Router();
const WeaponController = require('../controllers/Weapon')

router.get('/', WeaponController.getWeapon);
router.get('/add', WeaponController.addFormWeapon)
router.post('/add', WeaponController.addWeapon)
// router.get('/delete/:id', WeaponController.deleteWeapon)
// router.get('/edit/:id', WeaponController.editFormWeapon)
// router.post('/edit/:id', WeaponController.editWeapon)
// router.get('/:id', WeaponController.findById)

module.exports = router;