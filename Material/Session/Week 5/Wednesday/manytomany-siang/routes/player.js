const { Router } = require('express');
const router = Router();
const PlayerController = require('../controllers/Player')

router.get('/', PlayerController.getPlayer);
router.get('/add', PlayerController.addFormPlayer)
router.post('/add', PlayerController.addPlayer)
// router.get('/delete/:id', PlayerController.deletePlayer)
// router.get('/edit/:id', PlayerController.editFormPlayer)
// router.post('/edit/:id', PlayerController.editPlayer)
// router.get('/:id', PlayerController.findById)

module.exports = router;