'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Player extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Player.belongsToMany(models.Weapon, { through: 'models.Inventory' });
    }
  };
  Player.init({
    name: {
      type : DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Name must be filled thanks."
        }
      }
    },
    info:{
      type : DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Information must be filled thanks."
        }
      }
    },
    image: {
      type : DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Image must be filled thanks."
        },
        isUrl : {
          msg : "Image must be URL format!"
        }
      }
    },
    level: {
      type : DataTypes.INTEGER,
      validate : {
        notEmpty: {
          msg : "Level must be filled thanks."
        },
        isNumeric : {
          msg : "Level must be a number."
        }
      }
    },
  }, {
    hooks : {
      beforeCreate(player, options){
        player.level = 1;
      }
    },
    sequelize,
    modelName: 'Player',
  });
  return Player;
};