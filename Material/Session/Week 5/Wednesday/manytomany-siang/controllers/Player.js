const { Player } = require('../models')

class PlayerController {
    static async getPlayer(req, res) {
        try {
            const result = await Player.findAll({
                order: [
                    ['id', 'ASC']
                ],
            })
            res.status(200).json(result);

        }
        catch (err) {
            res.status(500).json(err);
        }
    }

    static addFormPlayer(req, res) {
        res.render('addPlayers.ejs');
    }
    static async addPlayer(req, res) {
        const { name, info, image, level } = req.body;
        try {
            const found = await Player.findOne({
                where: {
                    name
                }
            })
            if (found) {
                res.status(409).json({
                    msg: "Name already exist! Try another name arigato."
                })
            } else {
                const player = await Player.create({
                    name, info, image, level
                })

                res.status(201).json(player)
            }
        } catch (err) {
            res.status(500).json(err)
        }
    }

    // static findById(req, res) {
    //     const id = req.params.id;
    //     Player.findOne({
    //         where: { id }
    //     })
    //         .then(result => {
    //             res.send(result)
    //         })
    //         .catch(err => {
    //             res.send(err)
    //         })
    // }

    // static deletePlayer(req, res) {
    //     const id = req.params.id;
    //     Player.destroy({
    //         where: { id }
    //     })
    //         .then(() => {
    //             // res.send("Deleted")
    //             res.redirect('/Players')
    //         })
    //         .catch(err => {
    //             res.send(err)
    //         })
    // }

    // static editFormPlayer(req, res) {
    //     const id = req.params.id;
    //     // console.log(id)
    //     Player.findOne({
    //         where: { id }
    //     })
    //         .then(result => {
    //             console.log(result)
    //             res.render('editPlayers.ejs', { Player: result });
    //         })
    //         .catch(err => {
    //             res.send(err);
    //         })
    // }

    // static editPlayer(req, res) {
    //     const id = req.params.id;
    //     const { name, status, bounty } = req.body;
    //     Player.update({
    //         name,
    //         status,
    //         bounty,

    //     }, {
    //         where: { id }
    //     })
    //         .then(result => {
    //             if (result[0] === 1) {
    //                 res.redirect('/Players')
    //             } else {
    //                 res.send('Update not done!')
    //             }
    //             // res.send(result)
    //         })
    //         .catch(err => {
    //             res.send(err)
    //         })
    // }
}

module.exports = PlayerController; 