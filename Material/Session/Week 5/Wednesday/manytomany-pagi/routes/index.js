const { Router } = require('express');
const router = Router();
const PlayerRoutes = require('./player')
// const WeaponRoutes = require('./weapon')

router.get('/', (req,res)=>{
    res.render('index.ejs')
});
router.use('/players', PlayerRoutes)
// router.use('/weapons', WeaponRoutes)

module.exports = router;