const { Pirate,Ship } = require('../models')

class PirateController {
    // static getPirate(req, res) {
    //     Pirate.findAll({
    //         order : [
    //             ['id', 'ASC']
    //         ],
    //         include : [
    //             Ship
    //         ]
    //     })
    //         .then(result => {
    //             // res.json(result);
    //             res.render('pirates.ejs', { pirates: result })
    //         })
    //         .catch(err => {
    //             console.log(err);
    //         })
    // }
    
    static async getPirate(req, res) {
       try {
           const result = await Pirate.findAll({
               order : [
                   ['id', 'ASC']
               ],
               include : [
                   Ship
               ]
           })
               res.status(200).json(result);
           
       }
       catch(err){
           
       }
    }
    
    static addFormPirate(req, res) {
        res.render('addPirates.ejs');
    }
    static addPirate(req, res) {
        const { name, status, bounty, ShipId } = req.body;

        Pirate.findOne({
            where : {
                name
            }
        })
        .then(found => {
            if(found){
                res.send("Name already exist! Try another name arigato.")
            }else {
                return Pirate.create({
                    name,
                    status,
                    bounty,
                    ShipId
                })
            }
        }) 
        .then(result => {
            // res.send(result);
            res.redirect('/pirates')
        })
        .catch(err => {
            res.send(err)
        })
           
    }

    static findById(req, res) {
        const id = req.params.id;
        Pirate.findOne({
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }

    static deletePirate(req, res) {
        const id = req.params.id;
        Pirate.destroy({
            where: { id }
        })
            .then(() => {
                // res.send("Deleted")
                res.redirect('/pirates')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static editFormPirate(req, res) {
        const id = req.params.id;
        // console.log(id)
        Pirate.findOne({
            where : { id }
        })
        .then(result => {
            console.log(result)
            res.render('editPirates.ejs', { pirate:result });
        })
        .catch(err=>{
            res.send(err);
        })
    }

    static editPirate(req, res) {
        const id = req.params.id;
        const { name, status, bounty } = req.body;
        Pirate.update({
            name,
            status,
            bounty,
          
        }, {
            where: { id }
        })
            .then(result => {
                if(result[0] === 1) {
                    res.redirect('/pirates')
                }else{
                    res.send('Update not done!')
                }
                // res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }
}

module.exports = PirateController; 