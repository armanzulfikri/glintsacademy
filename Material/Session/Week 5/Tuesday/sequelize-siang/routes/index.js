const { Router } = require('express');
const router = Router();
const ShipsRoutes = require('./ship')
const PiratesRoutes = require('./pirate')

router.get('/', (req,res)=>{
    res.render('index.ejs')
});
router.use('/ships', ShipsRoutes)
router.use('/pirates', PiratesRoutes)

module.exports = router;