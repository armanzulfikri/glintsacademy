const express = require('express');
const app = express();
require('dotenv').config()

const PORT = process.env.PORT || 3000;

const router = require('./routes')

//Middlewares
app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({ extended: false}));

//Routes
app.use(router);

app.listen(PORT, () => {
    console.log(`Server is running at port : ${PORT}`);
})

// Step by step
/**
 * 1. Npm init
 * 2. Npm install 3rd party module -> .gitignore
 * 3. App.js -> untuk express, routes dan middlewares
 * 4. Routes setting
 * 5. Sequelize-cli init
 * 6. Connect ke PG
 * 7. Generate model, migration, seed(optional)
 * 8. Testing menggunakan Postman
 * 9. Ejs setting 
 * 10. Controller
 */