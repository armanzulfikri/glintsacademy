'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Pirate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Pirate.belongsTo(models.Ship)

    }
  };
  Pirate.init({
    name: DataTypes.STRING,
    status: DataTypes.STRING,
    bounty: DataTypes.INTEGER,
    ShipId : DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Pirate',
  });
  return Pirate;
};