'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   const parseData = JSON.parse(fs.readFileSync('./data.json'));
   const studentsData = [];

   pasrsedata.forEach(data => {
     const {name,major,score} = data;
     studentsData.push({
       name,
       major,
       score,
       creadeAt = new Date(),
       updateAt = new Date()
     })
   })
   await queryInterface.bulkInsert('student', studentsData,{});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('student', null, {});
  }
};
