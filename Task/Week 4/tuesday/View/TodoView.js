class View {
    static list(data){

        data.forEach(el => {
            console.log(`${el.id}. ${el.task}, status ${el.status} ${el.tag}.`);
        });
    }

    // static help(data){

    // }

    // static add(data){

    // }

    // static update(data){

    // }

    // static delete(data){

    // }

    // static complete(data){

    // }

    // static uncomplete(data){

    // }

    static message(data){
        console.log(data);
    }
}

module.exports = View;


